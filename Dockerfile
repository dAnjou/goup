FROM golang:1-alpine

RUN apk update && apk add --no-cache --quiet \
    gomplate

ENV temp_dir /tmp/temp
RUN mkdir ${temp_dir}

ADD https://github.com/goreleaser/nfpm/releases/download/v2.3.1/nfpm_2.3.1_Linux_x86_64.tar.gz ${temp_dir}/nfpm.tar.gz
RUN tar -C /usr/local/bin -xf ${temp_dir}/nfpm.tar.gz nfpm

ARG version

ENV source_dir /go/src/goup
ENV build_dir /public/builds
RUN mkdir -p ${source_dir} ${build_dir}

# build Goup
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 0
COPY . ${source_dir}
WORKDIR ${source_dir}
RUN go build -o /usr/local/bin/goup -ldflags "-X main.VERSION=${version}" -v .

# create tarball
WORKDIR ${temp_dir}
ENV tarball_name goup_${version}_${GOARCH}
RUN mkdir ${tarball_name}
RUN cp /usr/local/bin/goup ${tarball_name}/goup
RUN tar -czf ${build_dir}/${tarball_name}.tar.gz ${tarball_name}/goup

# create packages
WORKDIR ${build_dir}
RUN nfpm --config="${source_dir}/nfpm.yaml" pkg --packager deb --target goup_${version}_${GOARCH}.deb
RUN nfpm --config="${source_dir}/nfpm.yaml" pkg --packager rpm --target goup_${version}_${GOARCH}.rpm

# create website
RUN mkdir ${temp_dir}/website_source
WORKDIR ${temp_dir}/website_source
ENV VERSION version.txt
RUN goup -version > ${VERSION}
ENV USAGE usage.txt
RUN goup -help 2> ${USAGE}
ENV BUILDS ${build_dir}
RUN gomplate --file ${source_dir}/index.html.gom --out /public/index.html

RUN rm -rf ${temp_dir}

EXPOSE 4000
ENV data_dir /data
RUN mkdir ${data_dir}
VOLUME ${data_dir}

CMD ["goup", "-dir", "${data_dir}"]
