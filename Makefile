GOUP_VERSION=$(shell echo 0.2.2)
GOUP_IMAGE ?= goup
GOUP_PORT ?= 4000
GOUP_DIR ?= $(PWD)

CONTAINER_CLIENT ?= docker

.PHONY: all
all:
	@egrep -h "\s#help: " $(MAKEFILE_LIST) | sed -e 's/:.*#help: /~/' | column -t -s'~'

.PHONY: image
image: # Build Docker image with name $GOUP_IMAGE (default: goup)
	$(CONTAINER_CLIENT) build --build-arg version=$(GOUP_VERSION) --tag $(GOUP_IMAGE) .

.PHONY: build
build: image #help: Build binary, tarball, packages (DEB, RPM) and website
	GOUP_CONTAINER=$$($(CONTAINER_CLIENT) create $(GOUP_IMAGE)) && \
	$(CONTAINER_CLIENT) cp $$GOUP_CONTAINER:/public . ; \
	$(CONTAINER_CLIENT) rm $$GOUP_CONTAINER

.PHONY: run
run: image #help: Run Goup on port $GOUP_PORT (default: 4000) and expose $GOUP_DIR (default: $PWD).
	$(CONTAINER_CLIENT) run --rm -v $(GOUP_DIR):/data -p $(GOUP_PORT):4000 $(GOUP_IMAGE)

.PHONY: clean
clean:
	rm -rf public builds
	$(CONTAINER_CLIENT) rmi -f golang:1-alpine
	$(CONTAINER_CLIENT) rmi -f $(GOUP_IMAGE)

.PHONY: test
