Goup
====

Goup is a small one-binary file server written in Go which also allows to upload
files. You can run it standalone or deploy it as FastCGI application.

For installation and usage see https://danjou.gitlab.io/goup/.
